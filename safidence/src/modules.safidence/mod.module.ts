import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { GuestKeyModule } from './guestKey/guestKey.module';

@Module({
  imports: [
    GuestKeyModule,
    MongooseModule.forRoot(
      'mongodb+srv://safidence:safidence_1004@cluster0-x1zs8.mongodb.net/safidenceProject?retryWrites=true&w=majority',
    ),
  ],
  controllers: [ ],
  providers: [ ],
})
export class ModModules {}
