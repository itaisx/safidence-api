import * as mongoose from 'mongoose';

export const GuestKeySchema = new mongoose.Schema({
  keyPassword: String,
  startDate: Date,
  endDate: Date,
  doorLockId: Number,
});
