import { Document } from 'mongoose';

export interface GuestKey extends Document {
  readonly keyPassword: string;
  readonly startDate: Date;
  readonly endDate: Date;
  readonly doorLockId: number;
}
