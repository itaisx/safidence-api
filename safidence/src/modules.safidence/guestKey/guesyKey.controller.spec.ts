import { Test, TestingModule } from '@nestjs/testing';
import { GuestKeyController } from './guestKey.controller';
import { GuestKeyService } from './guestKey.service';

describe('AppController', () => {
  let appController: GuestKeyController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [GuestKeyController],
      providers: [GuestKeyService],
    }).compile();

    appController = app.get<GuestKeyController>(GuestKeyController);
  });

  // describe('root', () => {
  //   it('should return "Hello World!"', () => {
  //     expect(appController.getHello()).toBe('Hello World!');
  //   });
  // });
});
