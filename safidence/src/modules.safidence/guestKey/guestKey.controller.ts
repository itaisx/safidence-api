import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { GuestKeyService } from './guestKey.service';
import { CreateGuestKeyDTO } from './dto/create-guestKey.dto';

@Controller('GuestKey')
export class GuestKeyController {
  constructor(private readonly guestKeyService: GuestKeyService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @Post('addKey')
  addGuestKey(@Body() createguestKeyDTO: CreateGuestKeyDTO) {
    const result = this.guestKeyService.insertKey(createguestKeyDTO);
    return result;
  }

  // example for get all
  @Get('getKey')
  getGuestKey() {
    const key = this.guestKeyService.getKey();
    return key;
  }

  @Get(':getKeyById')
  getGuestKeyById(@Param('getKeyById') doorlockId: number) {
    return this.guestKeyService.getGuestKeyById(doorlockId);
  }
}
