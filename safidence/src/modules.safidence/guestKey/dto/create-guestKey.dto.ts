export class CreateGuestKeyDTO {
  readonly keyPassword: string;
  readonly startDate: Date;
  readonly endDate: Date;
  readonly doorLockId: number;
  // more fields at this point
}
