import { Module } from '@nestjs/common';
import { GuestKeyController } from './guestKey.controller';
import { GuestKeyService } from './guestKey.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GuestKeySchema } from './schema/guestKey.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'GuestKey', schema: GuestKeySchema }])],
  controllers: [GuestKeyController],
  providers: [GuestKeyService],
})
export class GuestKeyModule {}
