import { CreateGuestKeyDTO } from './dto/create-guestKey.dto';
import { Injectable, Body, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GuestKey } from './interfaces/guestKey.interfaces';

@Injectable()
export class GuestKeyService {
  private readonly guestKey: GuestKey[] = [];

  constructor(
    @InjectModel('GuestKey') private readonly guestKeyModel: Model<GuestKey>,
  ) {}

  // getHello(): string {
  //   return 'Hello World!';
  // }

  async insertKey(createGuestKeyDTO: CreateGuestKeyDTO) {
    const createGuestKey = new this.guestKeyModel(createGuestKeyDTO);
    return await createGuestKey.save();
  }

  async getKey() {
    const result = await this.guestKeyModel.find().exec();
    return result as GuestKey[];
  }

  // example where
  async getGuestKeyById(doorLockId: number): Promise<GuestKey> {
    const keyResult = await this.guestKeyModel.find({ doorLockId });
    if (!keyResult) {
      throw new NotFoundException('Cloud not find key');
    }

    return keyResult;
  }
}
