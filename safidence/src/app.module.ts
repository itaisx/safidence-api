import { Module } from '@nestjs/common';
import { ModModules } from './modules.safidence/mod.module';

@Module({
  imports: [
    ModModules,
  ],
  controllers: [  ],
  providers: [ ],
})
export class AppModule {}
